﻿using System.Collections.Generic;

namespace UtilitarioCambioMoneda.Ccl {
    public interface ICambioMonedaNav {
        bool ActualizarEmpresasABsS(IEnumerable<EmpresaCambioMoneda> valEmpresas);
        IEnumerable<EmpresaCambioMoneda> ObtenerEmpresasNoReconvertidas();
    }
}