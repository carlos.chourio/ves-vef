﻿namespace UtilitarioCambioMoneda.Ccl {
    public class EmpresaCambioMoneda {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string MonedaActual { get; set; }
        public bool ConvertirABsS { get; set; }
    }
}
