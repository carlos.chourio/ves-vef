﻿using System;
using System.Collections.Generic;

namespace UtilitarioCambioMoneda.Lib {
    public class LectorINIGalac {
        private string path;

        public LectorINIGalac(string path) {
            this.path = path;
        }

        public string ObtenerValorSeccion(string valSeccion) {
            string vResult = string.Empty;
            string[] lines = System.IO.File.ReadAllLines(path);
            foreach (var line in lines) {
                if (!string.IsNullOrEmpty(line)) {
                    int index1 = line.IndexOf("[");
                    int index2 = line.IndexOf("]");
                    string key = line.Substring(index1 + 1, index2 - index1 - 1);
                    string value = line.Substring(index2 + 4, line.Length - index2 - 5);
                    if (key.Equals(valSeccion, StringComparison.InvariantCultureIgnoreCase)) {
                        return value;
                    }
                }
            }
            return vResult;
        }
    }
}