﻿using System;
using System.Windows.Input;

namespace UtilitarioCambioMoneda.Lib {
    public class RelayCommand<T> : ICommand {
        public Action<T> execute { get; private set; }

        public Func<T, bool> canExecute { get; private set; }

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute) {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter) {
            return (canExecute == null || canExecute((T)parameter));
        }

        public void Execute(object parameter) {
            execute.Invoke((T)parameter);
        }

        public void RaiseCanExecuteChanged() {
            CommandManager.InvalidateRequerySuggested();
        }  
    }
    public class RelayCommand : ICommand {
        public Action execute { get; set; }

        public Func<bool> canExecute { get; set; }

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action execute, Func<bool> canExecute) {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter) {
            return (canExecute == null || canExecute());
        }

        public void Execute(object parameter) {
            execute.Invoke();
        }

        public void RaiseCanExecuteChanged() {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}