﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using UtilitarioCambioMoneda.Ccl;

namespace UtilitarioCambioMoneda.Brl {
    public class CambioMonedaNav : ICambioMonedaNav {
        private string conexionSQL;

        public CambioMonedaNav(string valNombreDb) {
            conexionSQL = ObtenerCadenaDeConexion(valNombreDb);
        }
        public IEnumerable<EmpresaCambioMoneda> ObtenerEmpresasNoReconvertidas() {
            IList<EmpresaCambioMoneda> Datos = new List<EmpresaCambioMoneda>();
            using (var vConexionSQL = new SqlConnection(conexionSQL)) {
                try {
                    vConexionSQL.Open();
                    SqlDataReader vLectorSQL;
                    string sentenciaSQL = "SELECT Codigo, Nombre, CodigoMoneda FROM dbo.Compania WHERE CodigoMoneda != 'VES'";
                    SqlCommand vCommand = new SqlCommand(sentenciaSQL, vConexionSQL);
                    vLectorSQL = vCommand.ExecuteReader();
                    while (vLectorSQL.Read()) {
                        string codigo = vLectorSQL.GetString(0);
                        string nombre = vLectorSQL.GetString(1);
                        string codigoMoneda = vLectorSQL.GetString(2);
                        Datos.Add(new EmpresaCambioMoneda { Codigo = codigo, Nombre = nombre, MonedaActual = codigoMoneda });
                    }
                } catch (Exception ex) {
                    throw new ArgumentException("No se pudo establecer la conexión con la Base de datos." + Environment.NewLine + ex.Message);
                }
            }
            return Datos;
        }

        public bool ActualizarEmpresasABsS(IEnumerable<EmpresaCambioMoneda> valEmpresas) {
            bool vResult = true;
            try {
                using (var vConexionSQL = new SqlConnection(conexionSQL)) {
                    foreach (var empresa in valEmpresas) {
                        using (var sqlCommand = vConexionSQL.CreateCommand()) {
                            sqlCommand.Parameters.AddWithValue("Codigo", empresa.Codigo);
                            sqlCommand.CommandText = "UPDATE dbo.Compania SET CodigoMoneda='VES' WHERE Codigo=@Codigo";
                            vConexionSQL.Open();
                            sqlCommand.ExecuteNonQuery();
                            vConexionSQL.Close();
                        }
                    }
                }
            } catch (Exception ex) {
                throw new ArgumentException("No se pudo establecer la conexión con la Base de datos." + Environment.NewLine + ex.Message);
            }
            return vResult;
        }

        private string ObtenerCadenaDeConexion(string valNombreDb) {
            string vResult = string.Empty;
            int index = valNombreDb.IndexOf('|');
            string dataSource = valNombreDb.Substring(0, index);
            string initialCatalog = valNombreDb.Substring(index + 2);
            vResult = "Data Source=" + dataSource + ";Initial Catalog=" + initialCatalog + ";Integrated Security=true";
            return vResult;
        }
    }
}
