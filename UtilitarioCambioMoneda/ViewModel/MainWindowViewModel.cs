﻿namespace UtilitarioCambioMoneda.Uil.ViewModel {
    public class MainWindowViewModel {
        public CambioMonedaViewModel CambioMonedaViewModel { get; set; }

        public MainWindowViewModel(CambioMonedaViewModel cambioMonedaViewModel) {
            this.CambioMonedaViewModel = cambioMonedaViewModel;
        }
    }
}
