﻿namespace Heimdall.InterfazUsuario.Libreria.UI
{
    public interface IServicioCuadroDialogo
    {
        bool MostrarCuadroDialogoAdvertenciaSiNo(string titulo, string mensaje);
        void MostrarCuadroDialogoError(string titulo, string mensaje);
        void MostrarCuadroDialogoMensaje(string titulo, string mensaje);
        void MostrarCuadroDialogoTareaCompletada(string titulo, string mensaje);
    }
}