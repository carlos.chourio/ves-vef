﻿using UtilitarioCambioMoneda.Ccl;
using UtilitarioCambioMoneda.Lib;

namespace UtilitarioCambioMoneda.Uil.ViewModel {
    public class EmpresaCambioMonedaViewModel : ObservableObject {
        private const string CodigoPropertyName = "Codigo";
        private const string NombrePropertyName = "Nombre";
        private const string ConvertirABsSPropertyName = "ConvertirABsS";
        private const string MonedaActualPropertyName = "MonedaActual";

        public EmpresaCambioMoneda Modelo { get; set; }

        public EmpresaCambioMonedaViewModel(EmpresaCambioMoneda modelo) {
            this.Modelo = modelo;
        }

        public string Codigo {
            get { return Modelo.Codigo; }
            set {
                Modelo.Codigo = value;
                RaisePropertyChanged(CodigoPropertyName);
            }
        }

        public string MonedaActual {
            get { return Modelo.MonedaActual; }
            set {
                Modelo.MonedaActual = value;
                RaisePropertyChanged(MonedaActualPropertyName);
            }
        }

        public string Nombre {
            get { return Modelo.Nombre; }
            set {
                Modelo.Nombre = value;
                RaisePropertyChanged(NombrePropertyName);
            }
        }

        public bool ConvertirABsS {
            get { return Modelo.ConvertirABsS; }
            set {
                Modelo.ConvertirABsS = value;
                RaisePropertyChanged(ConvertirABsSPropertyName);
            }
        }
    }
}
