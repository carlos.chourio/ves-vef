﻿using UtilitarioCambioMoneda.Uil.View.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using UtilitarioCambioMoneda.Ccl;
using UtilitarioCambioMoneda.Lib;
using UtilitarioCambioMoneda.Brl;

namespace UtilitarioCambioMoneda.Uil.ViewModel {
    public class CambioMonedaViewModel : ObservableObject {
        #region Constantes
        private const string EmpresasPropertyName = "Empresas";
        private const string EmpresaSeleccionadaPropertyName = "EmpresaSeleccionada";
        #endregion

        #region Variables Privadas
        private ObservableCollection<EmpresaCambioMonedaViewModel> empresas;
        private EmpresaCambioMonedaViewModel empresaSeleccionada;
        private ICambioMonedaNav cambioMonedaNav;
        private IServicioCuadroDialogo cuadroDialogo;
        #endregion

        #region Propiedades
        public ObservableCollection<EmpresaCambioMonedaViewModel> Empresas {
            get { return empresas; }
            set {
                if (empresas != value) {
                    empresas = value;
                    RaisePropertyChanged(EmpresasPropertyName);
                }
            }
        }
        public EmpresaCambioMonedaViewModel EmpresaSeleccionada {
            get { return empresaSeleccionada; }
            set {
                if (empresaSeleccionada != value) {
                    empresaSeleccionada = value;
                    RaisePropertyChanged(EmpresaSeleccionadaPropertyName);
                }
            }
        }
        public ICommand ProcesarCambioDeMonedaCommand { get; set; }
        public ICommand ConectarDBCommand { get; set; }
        #endregion

        #region Constructor
        public CambioMonedaViewModel(IServicioCuadroDialogo servicioCuadroDialogo) {
            this.cuadroDialogo = servicioCuadroDialogo;
            Empresas = new ObservableCollection<EmpresaCambioMonedaViewModel>();
            InicializarCommands();
        }
        #endregion

        private void InicializarCommands() {
            ProcesarCambioDeMonedaCommand = new RelayCommand(ExecuteCambiarMoneda, CanExecuteCambiarMoneda);
            ConectarDBCommand = new RelayCommand(ExecuteConectarDB, () => true);
        }

        #region Executes
        private void ExecuteConectarDB() {
            string vNombreDb = LeerINI();
            if (!string.IsNullOrEmpty(vNombreDb)) {
                cambioMonedaNav = new CambioMonedaNav(vNombreDb);
                CargarEmpresasNoReconvertidas();
            } else {
                cuadroDialogo.MostrarCuadroDialogoError("Error de archivo de configuracion", "El archivo de configuracion no se encuentra en la ruta esperada");
            }
        }

        private bool CanExecuteCambiarMoneda() {
            return true; //Empresas!=null && Empresas.Any(t => t.ConvertirABsS);
        }

        private void ExecuteCambiarMoneda() {
            try {
                if (Empresas.Count(t => t.ConvertirABsS) == 0) {
                    cuadroDialogo.MostrarCuadroDialogoError("Error", "Debe seleccionar al menos una empresa");
                    return;
                }
                cambioMonedaNav.ActualizarEmpresasABsS(ObtenerEmpresasAReconvertir());
                cuadroDialogo.MostrarCuadroDialogoMensaje("Moneda Actualizada", "La actualizacion a VES se ha completado satisfactoriamente");
                CargarEmpresasNoReconvertidas();
            } catch (Exception ex) {
                cuadroDialogo.MostrarCuadroDialogoError("Error en DB", ex.Message);
            }
        }
        #endregion

        private string LeerINI() {
            string vResult = string.Empty;
            try {
                string vPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\Galac\\SAW\\SAWDB.INI";
                var lector = new LectorINIGalac(vPath);
                vResult = lector.ObtenerValorSeccion("servidor");
            } catch (Exception ex) {
                cuadroDialogo.MostrarCuadroDialogoError("Error en archivo .INI", ex.Message);
            }
            return vResult;
        }

        private void CargarEmpresasNoReconvertidas() {
            try {
                if (Empresas != null) {
                    foreach (var empresa in Empresas) {
                        empresa.PropertyChanged -= EmpresaViewModel_PropertyChanged;
                    }
                    Empresas.Clear();
                }
                IEnumerable<EmpresaCambioMoneda> empresasNoReconvertidas = cambioMonedaNav.ObtenerEmpresasNoReconvertidas();
                foreach (var empresa in empresasNoReconvertidas) {
                    EmpresaCambioMonedaViewModel empresaViewModel = new EmpresaCambioMonedaViewModel(empresa);
                    empresaViewModel.PropertyChanged += EmpresaViewModel_PropertyChanged;
                    Empresas.Add(empresaViewModel); 
                }
                
            } catch (Exception ex) {
                cuadroDialogo.MostrarCuadroDialogoError("Error en DB", ex.Message);
            }
        }

        private void EmpresaViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(EmpresaCambioMonedaViewModel.ConvertirABsS)) {
                ((RelayCommand)ProcesarCambioDeMonedaCommand).RaiseCanExecuteChanged();
            }
        }

        private IEnumerable<EmpresaCambioMoneda> ObtenerEmpresasAReconvertir() {
            return Empresas.Where(t => t.ConvertirABsS).Select(t => new EmpresaCambioMoneda() {
                Codigo=t.Codigo
            });
        }
    }
}
