﻿using System.Windows;

namespace UtilitarioCambioMoneda.Uil.View.Service
{
    public class ServicioCuadroDialogo : IServicioCuadroDialogo
    {
        public bool MostrarCuadroDialogoAdvertenciaSiNo(string titulo, string mensaje)
        {
            MessageBoxResult resultado = MessageBox.Show(mensaje, titulo, MessageBoxButton.YesNo, MessageBoxImage.Warning);
            return (resultado == MessageBoxResult.Yes);
        }

        public void MostrarCuadroDialogoMensaje(string titulo, string mensaje)
        {
            MessageBox.Show(mensaje, titulo, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void MostrarCuadroDialogoError(string titulo, string mensaje)
        {
            MessageBox.Show(mensaje, titulo, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void MostrarCuadroDialogoTareaCompletada(string titulo, string mensaje)
        {
            MessageBox.Show(mensaje, titulo, MessageBoxButton.OK, MessageBoxImage.Hand);
        }
    }
}
