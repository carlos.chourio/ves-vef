﻿using System.Windows;
using UtilitarioCambioMoneda.Uil.ViewModel;

namespace UtilitarioCambioMoneda
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel MainWindowViewModel { get; set; }

        public MainWindow(MainWindowViewModel mainWindowViewModel)
        {
            InitializeComponent();
            this.MainWindowViewModel = mainWindowViewModel;
            this.DataContext = MainWindowViewModel;
        }
    }
}
