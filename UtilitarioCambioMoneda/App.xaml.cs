﻿using UtilitarioCambioMoneda.Uil.View.Service;
using System.Windows;

namespace UtilitarioCambioMoneda
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindow window = new MainWindow(new Uil.ViewModel.MainWindowViewModel(
                new Uil.ViewModel.CambioMonedaViewModel(
                    new ServicioCuadroDialogo())));
            window.Show();
        }
    }
}
